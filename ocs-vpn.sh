#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)
tag="# Generated by $(basename "${BASH_SOURCE[0]}" .sh)"
os=$(grep -oP '(?<=^ID=).+' /etc/os-release | sed 's/"//g')

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        [[ -f $BASH_USER_LIB ]] || {
            local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
            type curl &>/dev/null || {
                echo "To continue, you should install curl"
                exit 1
            }
            curl --silent $SRC --output "$BASH_USER_LIB"
        }
    fi
    source "$BASH_USER_LIB"
}

usage()
{
    cat <<EOF
    Usage: $(basename "${BASH_SOURCE[0]}") -d {dir_in_sets} [-r] [-s [run]] [-c] [-h] [-v]

    Need write something

    Available options:

    -d, --directory Install settings from ./sets/<dir>
    -r, --remove    Remove all configs you must set '-d' parametr
    -s, --server    Create key, crt for server
    -c, --client    Create key, crt and ovpn for client
    -h, --help      Print this help and exit
    -v, --verbose   Print script debug info
    --no-color
EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
    [[ -z ${is_remove-} ]] || {
        if systemctl --quiet is-active ${sys_service//@/@$SERVER_NAME}; then
            msg "[-] stop ${sys_service//@/@$SERVER_NAME}...\c"
            systemctl --quiet disable --now ${sys_service//@/@$SERVER_NAME}
            msg "\r${GREEN}[+]${NOFORMAT}"
        fi

        msg "[-] remove config...\c"
        rm --force $(find /etc/openvpn/ -type f -name $SERVER_NAME* -exec grep "$tag" -l {} \;)
        for path in $(cd "$tmp"; ls -d */); do
            rm --force $(grep "$tag" -R -l "/${path//-/\/}" 2>/dev/null)
        done
        msg "\r${GREEN}[+]${NOFORMAT}"
    }
    [[ -d $tmp ]] && rm --recursive --force $tmp
    exit
}

load_config()
{
    oldIFS=$IFS IFS='|'
    for file in $1; do
        [[ -f $file ]] && source "$file"
    done
    IFS=$oldIFS
}

parse_params()
{
    tmp=$(mktemp -d)
    sys_service="ocs-vpn@.service"
    while :; do
        case "${1-}" in
            -d | --directory)
                export SERVER_NAME="${2-}"
                shift
                ;;
            -s | --server)
                is_server=true
                [[ ${2-} != run ]] || {
                    is_server_run=true
                    shift
                }
                ;;
            -c | --client) is_client=true ;;
            -r | --remove)
                is_remove=1
                ;;
            -h | --help) usage ;;
            -v | --verbose)
                set -x
                PS4='$(basename $0) [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1 ;;
            -?*) die "Unknown option: $1" ;;
            *) break ;;
        esac
        shift
    done

    [[ -z ${SERVER_NAME-} ]] && die "Missing required parameter: ./sets/<dir>"
    [[ -n ${is_server-} || -n ${is_client-} || -n ${is_server_run-} ]] || {
        is_server=true is_client=true is_server_run=true
    }

    ###   load config for remove only one server's config   ###
    cp --recursive --force "$script_dir/sets/$SERVER_NAME"/* $tmp
    local config_files=(
        "$tmp/ocs-vpn.conf"
        "/etc/openvpn/server/${SERVER_NAME}-ocs-vpn.conf"
    )
    load_config "$(printf "%s|" "${config_files[@]}")"
    [[ -z ${is_remove-} ]] || cleanup

    return 0
}

install_openvpn()
{
    export PATH=/usr/share/easy-rsa:/usr/share/easy-rsa/3:$PATH
    if check_app openvpn easyrsa; then
        msg "${GREEN}Apps already installed, skip...${NOFORMAT}"
        return 0
    fi
    msg "[-] Install apps...\c"
    case $os in
        rhel|centos)
                    dnf -y upgrade
                    dnf -y install epel-release
                    dnf install -y openvpn easy-rsa
                    ;;
        debian)
                    apt update
                    apt -y upgrade
                    apt -y install openvpn easy-rsa
                    ;;
        gentoo)
                    emerge --sync
                    emerge --deep --newuse --update
                    emerge net-vpn/openvpn app-crypt/easy-rsa
                    ;;
        *)          die "cannot check type os: \"$os\"" ;;
    esac
    msg "${GREEN}\r[+]${NOFORMAT}"
}

setup_easyrsa()
{
    msg "${GREEN}Prepare easy-rsa...${NOFORMAT}"

    create_dir "$EASYRSA"
    [[ -d $EASYRSA/pki ]] || easyrsa init-pki

    ###   create_vars   ###
    export EASYRSA_VARS_FILE="$EASYRSA/pki/vars"
    cat <<-EOF > $EASYRSA_VARS_FILE
		set_var EASYRSA_REQ_COUNTRY  "$EASYRSA_REQ_COUNTRY"
		set_var EASYRSA_REQ_PROVINCE "$EASYRSA_REQ_PROVINCE"
		set_var EASYRSA_REQ_CITY     "$EASYRSA_REQ_CITY"
		set_var EASYRSA_REQ_ORG      "$EASYRSA_REQ_ORG"
		set_var EASYRSA_REQ_EMAIL    "$EASYRSA_REQ_EMAIL"
		set_var EASYRSA_REQ_OU       "$EASYRSA_REQ_OU"
EOF

    ###   create CA   ###
    [[ -f $EASYRSA/pki/ca.crt ]]  || easyrsa --batch build-ca
    [[ -f $EASYRSA/pki/crl.pem ]] || easyrsa --batch gen-crl
}

check_user_group()
{
    if [[ -z $(grep $SERVER_USER /etc/passwd) ]]; then
        useradd --no-create-home --shell $(find / -type f -name "nologin") --uid 65534 $SERVER_USER
    fi
    export SERVER_GROUP=$(id -ng $SERVER_USER)
}

check_config_server()
{
    local check_list_file="-ca.crt .conf .crt -dh.pem .key -tc.key"
    local missing_files=
    for suffix in $check_list_file; do
        [[ -f $SERVER_CONF/$SERVER_NAME$suffix ]] || missing_files+="$SERVER_NAME$suffix "
    done
    [[ -z $missing_files ]] || return 1
    return 0
}

setup_server()
{
    check_user_group
    if check_config_server; then
        read -r -p "All config files for $SERVER_NAME exist. Do you want to rebuild it? (yes): "
        [[ $REPLY == yes ]] || return 0
    fi

    msg "${GREEN}Setup server...${NOFORMAT}"
    create_dir "$SERVER_CONF"

    ###   create, copy keys and config for server   ###
    [[ -f $EASYRSA/pki/private/$SERVER_NAME.key ]] || easyrsa --batch gen-req $SERVER_NAME nopass
    [[ -f $EASYRSA/pki/issued/$SERVER_NAME.crt ]]  || easyrsa --batch sign-req server $SERVER_NAME
    [[ -f $EASYRSA/pki/dh.crt ]]                   || easyrsa gen-dh
    [[ -f $EASYRSA/pki/tc.crt ]]                   || openvpn --genkey secret "$EASYRSA/pki/tc.key"

    cp "$EASYRSA/pki/ca.crt" "$SERVER_CONF/$SERVER_NAME-ca.crt"
    if [[ $os =~ (centos|rhel) ]]; then
        ###   remove an empty password for server
        openssl rsa -in "$EASYRSA/pki/private/$SERVER_NAME.key" -out "$SERVER_CONF/$SERVER_NAME.key"
    else
        mv "$EASYRSA/pki/private/$SERVER_NAME.key" "$SERVER_CONF"
    fi
    mv "$EASYRSA/pki/issued/$SERVER_NAME.crt" "$SERVER_CONF"
    mv "$EASYRSA/pki/dh.pem" "$SERVER_CONF/$SERVER_NAME-dh.pem"
    mv "$EASYRSA/pki/tc.key" "$SERVER_CONF/$SERVER_NAME-tc.key"
    cat "$tmp/server.conf" | envsubst > "$SERVER_CONF/$SERVER_NAME".conf
    local cp_opt="--update=none"
    local cp_ver=$(cp --version | head -n 1 | cut -d' ' -f 4)
    if awk "BEGIN {exit !($cp_ver <= 9.1)}" ; then
        cp_opt="--no-clobber"
    fi
    cp $cp_opt "$tmp/ocs-vpn.conf" "$SERVER_CONF/$SERVER_NAME-ocs-vpn.conf"

    add_tag "$SERVER_CONF" "$SERVER_NAME"

    ###   install systemd units and other tools   ###
    for path_from_dname in $(cd "$tmp"; ls -d */); do
        local dst="/${path_from_dname//-/\/}"
        local src="$tmp/$path_from_dname"
        create_dir "$dst"
        [[ $path_from_dname =~ usr-bin ]] || add_tag "$src"
        cp --recursive --force "$src"/* "$dst"
    done
}

setup_user()
{
    msg "${GREEN}Setup user...${NOFORMAT}"
    create_dir "$USER_CONF"

    ###   copy *-server.ca and *-server.tc.key   ###
    cp "$SERVER_CONF/$SERVER_NAME-ca.crt" "$USER_CONF"
    cp "$SERVER_CONF/$SERVER_NAME-tc.key" "$USER_CONF"

    ###   create key for all user   ###
    for user in $USER_LIST; do
        local srv_cli=$SERVER_NAME-$user

        ###   create *.conf   ###
        cat "$tmp/client.conf" | CLIENT_NAME=$srv_cli envsubst > "$USER_CONF/$srv_cli.conf"

        ###   create *.key, *.crt   ###
        easyrsa --batch gen-req $srv_cli nopass
        easyrsa --batch sign-req client $srv_cli
        mv "$EASYRSA/pki/"{issued/$srv_cli.crt,private/$srv_cli.key} "$USER_CONF"

        umask 0077
        {   ###   create *.ovpn   ###
                                sed '1,5d' "$USER_CONF/$srv_cli.conf"
            echo "<ca>";        sed '1,2d' "$USER_CONF/$SERVER_NAME-ca.crt";   echo "</ca>"
            echo "<cert>";      awk '/BEGIN/,/END/' "$USER_CONF/$srv_cli.crt"; echo "</cert>"
            echo "<key>";       cat "$USER_CONF"/$srv_cli.key;                 echo "</key>"
            echo "<tls-crypt>"; sed '1,2d' "$USER_CONF/$SERVER_NAME-tc.key";   echo "</tls-crypt>"
        } > "$USER_CONF"/$srv_cli.ovpn
        umask 0022
    done

    add_tag "$USER_CONF" "$SERVER_NAME"
}

start_server()
{
    msg "[-] Start ${sys_service//@/@$SERVER_NAME}...\c"
    create_dir "/var/log/openvpn"
    #[[ $(getenforce 2>/dev/null) != Enforcing ]] || {
    #    [[ $(semanage port --list | grep $SERVER_PORT | cut -d' ' -f 1) ==  openvpn_port_t ]] || {
    #        semanage port -a -t openvpn_port_t -p tcp $SERVER_PORT
    #    }
    #}
    systemctl daemon-reload
    systemctl --quiet enable --now ${sys_service//@/@$SERVER_NAME}
    msg "${GREEN}\r[+]${NOFORMAT}"
}

load_lib
setup_colors
parse_params "$@"

check_root
install_openvpn
setup_easyrsa

[[ -z ${is_server-} ]]     || setup_server
[[ -z ${is_client-} ]]     || setup_user
[[ -z ${is_server_run-} ]] || start_server
