# OCS-VPN

This script automates the installation of OpenVPN and Easy-RSA, generates keys for both the server and clients, and starts the server.


## Configuration

Your configuration files should be located in `./sets/<your_dir>`.
```
sets/
└── <your_dir>
    ├── ocs-vpn.conf
    ├── path-to-system
    │   ├── script.sh
    └── ...
```

* Edit `./sets/your_dir/ocs-vpn.conf` before executing the script.
* Note that the script does not set up a password for the server. However, you can set it up for CA use by modifying `EASYRSA_PASS[IN|OUT]`.
* If you are using CentOS, select a port other than those listed in the output of the command `semanage port -l`. Otherwise run `semanage port -a -t openvpn_port_t -p <VPN_PROTO> <SERVER_POR`
* To install additional tools on the system, place your script in a directory named according to `path-to-system` in `man systemd.mount.5`.
* The script will add a tag to the top of each file to easily remove all configs from `/etc/openvpn/*` and any additional directories set up in the name.


## Usage


`./ocs-vpn -d home`

Install the settings from `./sets/home`.

`./ocs-vpn -r -d home`

Remove the settings `home` from system.

`./ocs-vpn -h`

for help


## License
MIT
